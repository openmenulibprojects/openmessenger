package it.lundstedt.erik.openMessenger;

import it.lundstedt.erik.menu.Menu;
import it.lundstedt.erik.openMessenger.encryption.Encryption;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

/*
 * The Client that can be run both as a console or a GUI
 */
public class Client  {

	// for I/O
	private ObjectInputStream sInput;		// to read from the socket
	private ObjectOutputStream sOutput;		// to write on the socket
	private Socket socket;
	// the server, the port and the username
	private String server, username;
	private int port;



	private boolean doDebug;
	/*
	 *  Constructor called by console mode
	 *  server: the server address
	 *  port: the port number
	 *  username: the username
	 */
	Client(String server, int port, String username,boolean debug) {

		this.server = server;
		this.port = port;
		this.username = username;
		this.doDebug = debug;
	}
	
	/*
	 * To start the dialog
	 */
	public boolean start() {
		// try to connect to the server
		try {
			socket = new Socket(server, port);
		}
		// if it failed not much I can so
		catch(Exception ec) {
			Menu.error("Error connectiong to server:" + ec);
			return false;
		}
		
		String msg = "Connection accepted " + socket.getInetAddress() + ":" + socket.getPort();
		Menu.drawItem(msg);
		
		/* Creating both Data Stream */
		try
		{
			sInput  = new ObjectInputStream(socket.getInputStream());
			sOutput = new ObjectOutputStream(socket.getOutputStream());
		}
		catch (IOException eIO) {
			Menu.error("Exception creating new Input/output Streams: " + eIO);
			return false;
		}
		
		// creates the Thread to listen from the server
		new ListenFromServer().start();
		// Send our username to the server this is the only message that we
		// will send as a String. All other messages will be ChatMessage objects
		try
		{
			sOutput.writeObject(username);
		}
		catch (IOException eIO) {
			Menu.error("Exception doing login : " + eIO);
			disconnect();
			return false;
		}
		// success we inform the caller that it worked
		return true;
	}

	/*
	 *
	 * To send a message to the server
	 */
	void sendMessage(ChatMessage msg) {
		try {
			sOutput.writeObject(msg);
		}
		catch(IOException e) {
			Menu.error("Exception writing to server: " + e);
		}
	}
	
	/*
	 * When something goes wrong
	 * Close the Input/Output streams and disconnect not much to do in the catch clause
	 */
	private void disconnect() {
		try {
			if(sInput != null) sInput.close();
		}
		catch(Exception e) {} // not much else I can do
		try {
			if(sOutput != null) sOutput.close();
		}
		catch(Exception e) {} // not much else I can do
		try{
			if(socket != null) socket.close();
		}
		catch(Exception e) {} // not much else I can do
		

		
	}
	/*
	 * To start the Client in console mode use one of the following command
	 * > java Client
	 * > java Client username
	 * > java Client username portNumber
	 * > java Client username portNumber serverAddress
	 * at the console prompt
	 * If the portNumber is not specified 1500 is used
	 * If the serverAddress is not specified "localHost" is used
	 * If the username is not specified "Anonymous" is used
	 * > java Client
	 * is equivalent to
	 * > java Client Anonymous 1500 localhost
	 * are eqquivalent
	 *
	 * In console mode, if an error occurs the program simply stops
	 * when a GUI id used, the GUI is informed of the disconnection
	 */
	public static void main(String[] args) {
		// default values
		int portNumber = 1500;
		String serverAddress = "localhost";
		String userName = "Anonymous";
		boolean doDebug=false;
		// depending of the number of arguments provided we fall through
		switch(args.length)
		{
			// > javac Client username portNumber serverAddr doDebug
			case 4:
				doDebug = Boolean.parseBoolean(args[3]);
			// > javac Client username portNumber serverAddr
			case 3:
				serverAddress = args[2];
				// > javac Client username portNumber
			case 2:
				try {
					portNumber = Integer.parseInt(args[1]);
				}
				catch(Exception e) {
					Menu.error("Invalid port number.");
					Menu.error("Usage is: > java Client [username] [portNumber] [serverAddress]");
					Menu.important(args[1]);
					Menu.debug(e.getMessage(),doDebug);
					return;
				}
				// > javac Client username
			case 1:
				userName = args[0];
				// > java Client
			case 0:
				break;
			// invalid number of arguments
			default:
				Menu.error("Usage is: > java Client [username] [portNumber] {serverAddress]");
				return;
		}
		// create the Client object
		Client client = new Client(serverAddress, portNumber, userName,doDebug);
		// test if we can start the connection to the Server
		// if it failed nothing we can do
		if(!client.start())
			return;
		
		// wait for messages from user
		Scanner scan = new Scanner(System.in);
		// loop forever for message from the user
		while(true) {
			System.out.print("> ");
			// read message from user
			String msg = scan.nextLine();
			// logout if message is LOGOUT
			if(msg.equalsIgnoreCase("LOGOUT")) {
				client.sendMessage(new ChatMessage(ChatMessage.LOGOUT, ""));
				// break to do the disconnect
				break;
			}
			// message WhoIsIn
			else if(msg.equalsIgnoreCase("WHOISIN")) {
				client.sendMessage(new ChatMessage(ChatMessage.WHOISIN, ""));
			}
			else {// default to ordinary message


				System.out.println(userName);
				String encrMsg= Encryption.encrypt(msg,"erik");
				System.out.println(encrMsg);
				client.sendMessage(new ChatMessage(ChatMessage.MESSAGE, encrMsg));
			}
		}
		// done disconnect
		client.disconnect();
	}
	
	/*
	 * a class that waits for the message from the server and simply System.out.println() it in the console
	 */
	class ListenFromServer extends Thread {

		public void run() {
			while(true) {
				try {
					String msg = (String) sInput.readObject();
					String[] messageArr={
							msg,
							msg.substring(username.length()+11),
							msg.substring(9,9+username.length()),

					};
					//Encryption.decrypt(msg.substring(username.length()+11),username)
					Menu.drawHeader(messageArr);
					System.out.println(
					Encryption.decrypt(
							msg.substring(username.length()+11),
							msg.substring(9,9+username.length())));

						System.out.println("> ");
				}
				catch(IOException e) {
					Menu.error("Server has closed the connection: " + e);
				}
				// can't happen with a String object but need the catch anyhow
				catch(ClassNotFoundException e2)
				{
				}
			}
		}
	}
}
